//: [Previous](@previous)

/*
 Las estructuras tienen dos ventajas sobre las tuplas:
 
 * los componentes siempre tienen nombre
 * pueden tener métodos
 
 */

struct Complex{
    let x : Double
    let y : Double
    
    // Inits
    init(real: Double, imaginary: Double){
        (x,y) = (real, imaginary)
    }
    
    init(real: Double){
        self.init(real: real, imaginary: 0)
    }
    
    init(imaginary: Double){
        self.init(real: 0, imaginary: imaginary)
    }
    
    // Operations
    func magnitude()->Double{
        return ((x * x) + (y * y)).squareRoot()
    }
}


let a = Complex(real: 3, imaginary: -4)
Complex(imaginary: 7)

// Structs & Inmutabilidad
// Por defecto, todo en una struct es inmutable
struct Point{
    var x : Double
    var y : Double
    
    mutating func moveTo(x: Double, y: Double){
       self.x = x
       self.y = y
    }
}

// Structs NUNCA se comparten
var x1 : Int = 32
var x2 = x1 // Aqui, se hizo una copia!
x1 = 0


// Structs NO tienen HERENCIA




//: [Next](@next)
