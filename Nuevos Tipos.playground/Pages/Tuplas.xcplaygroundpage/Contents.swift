//: [Previous](@previous)


//: Función con más de un valor de retorno
func intDiv(_ a: Int, _ b: Int) -> (cociente: Int, resto: Int){
    return ((a / b), (a % b))
}

intDiv(3, 2).cociente

// Un servidor Web recibe un request y devuelve un response

typealias HTTPStatus = (code: Int, text: String)
typealias HTTPRequest = String
typealias HTTPResponse = (body: String, status: HTTPStatus)

func httpServer(request: HTTPRequest)-> HTTPResponse{
    // Habría que poner el código de verdad
    return ("It Worked!", (200, "OK"))
}

let response = httpServer(request: "index.html")
dump(response)

// Usamos "pattern matching" para "desensamblar" la tupla en varias variables
let (cociente, resto) = intDiv(5, 2)
dump(cociente)
dump(resto)

let (_, status) = httpServer(request: "index.php")
status
// extraemos el código
let (code, _) = status
dump(code)

// Tuplas de n elementos
// a la tupla de n elementos, se le llama n-Tupla
(3,4, "fjfjfj") // 3-tupla
(3,4) // 2-tupla
(3)   // 1-tupla? Pues no.
() // 0-tupla? Pues si.

// La cero tupla significa la nada. Es como void en otros lenguajes.

// Una función que no devuelve nada, en realidad devuelve la ().

// Estas dos funciones son iguales
func f(a:Int){
    print(a)
}

func h(a:Int) -> (){
    print(a)
}

let nothing = f(a: 4)


//: [Next](@next)
