
// Valores, "bindings" & REPL
let answer = 40 + 2

// Siempre mejor let (inmutable) que var (variable)
var donnaMobile = "Soy una variable y puedo combiar"
donnaMobile = "Epetekan"

// Podemos definir de forma explícita el tipo de una variable
let name : String = "Jon Snow"

// Inferencia de tipos: común en lenguajes como Haskell, OCaml, F#, Kotlin, Idris, etc... Usease, lenguajes funcionales con tipado estático
let name2 = "Tyrion"

// Un alias para un tipo
// typealias NombreNuevo = NombreViejo

typealias Integer = Int

let a : Integer = 34
type(of:a)

typealias Author = String
let grrm : Author = "George Raymond Richard Martin"

// El símbolo anónimo _
let _ = "Robb Stark"
let _ = "Stannis"
// Tanto Stannis como Robb, se han ido al pedo

// Intro a funciones
func add(x: Int, y: Int) -> Int{
    return x + y
}
add(x: 44, y: 2)

// Parámetros con nombres internos y externos
func fractionOf(numerator a: Double,
                denominator b: Double)->Double{
    return a / b
}

fractionOf(numerator: 3.2, denominator: 42)

// Parámetros sin nombres externos
func add42(_ a: Int)->Int{
    return a + 42
}
add42(5)














